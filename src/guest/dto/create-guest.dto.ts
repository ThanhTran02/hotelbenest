export class CreateGuestDto {
  bookingID: number;
  GusestID: number;
  RoomNumber: number;
  CheckinDate: Date;
  CheckoutDate: Date;
  TotalPrice: number;
}
