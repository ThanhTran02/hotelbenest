import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  Req,
  Put,
} from '@nestjs/common';
import { GuestService } from './guest.service';
import { CreateGuestDto } from './dto/create-guest.dto';
import { UpdateGuestDto } from './dto/update-guest.dto';
import { Request, Response, response } from 'express';

@Controller('api/v1/guest')
export class GuestController {
  constructor(private readonly guestService: GuestService) {}

  @Post()
  async create(
    @Body() createGuestDto: CreateGuestDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.guestService.create(createGuestDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get()
  async findAll(@Req() req: Request, @Res() res: Response): Promise<any> {
    const result = await this.guestService.findAll();
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get(':id')
  async findOne(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.guestService.findOne(+id);

    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
  @Get('search/:name')
  async searchUserByName(
    @Param('name') name: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.guestService.searchUserByName(name);

    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateGuestDto: UpdateGuestDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.guestService.update(+id, updateGuestDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Delete(':id')
  async remove(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    await this.guestService.remove(+id);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
    });
  }
}
