import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateGuestDto } from './dto/create-guest.dto';
import { UpdateGuestDto } from './dto/update-guest.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class GuestService {
  constructor(private prisma: PrismaService) {}
  create(createGuestDto: any) {
    return this.prisma.guest.create({ data: createGuestDto });
  }

  findAll() {
    return this.prisma.guest.findMany({
      where: { IsDeleted: false },
      include: { GuestType: true },
    });
  }

  async findOne(id: number) {
    return this.prisma.guest.findMany({
      where: { GuestID: id, IsDeleted: false },
      include: { GuestType: true },
    });
  }

  searchUserByName(name: string) {
    return this.prisma.guest.findMany({
      where: {
        Name: {
          contains: name,
        },
      },
      include: { GuestType: true },
    });
  }

  async update(id: number, updateGuestDto: any) {
    const checkId = await this.prisma.guest.findUnique({
      where: { GuestID: id, IsDeleted: false },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.guest.update({
      where: { GuestID: id },
      data: updateGuestDto,
    });
  }

  async remove(id: number) {
    console.log(id);

    const checkId = await this.prisma.guest.findMany({
      where: { GuestID: id, IsDeleted: false },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.guest.update({
      where: { GuestID: id },
      data: { IsDeleted: true },
    });
  }
}
