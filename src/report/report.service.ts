import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class ReportService {
  constructor(private prisma: PrismaService) {}

  async getRevenueByMonthAndYear(month: number, year: number): Promise<number> {
    const startDate = new Date(year, month - 1, 1);
    const endDate = new Date(year, month, 0); // last day of the month

    const totalRevenue = await this.prisma.booking.aggregate({
      _sum: {
        TotalPrice: true,
      },
      where: {
        CheckinDate: {
          gte: startDate,
          lte: endDate,
        },
        IsPaid: true,
      },
    });
    //@ts-ignore
    return totalRevenue._sum.TotalPrice || 0;
  }
  //lay doanh thu cac loai phong theo nam
  async getRevenueByYear(year: number): Promise<any> {
    const startDate = new Date(year, 0, 1);
    const endDate = new Date(year + 1, 0, 1);

    const bookings = await this.prisma.booking.findMany({
      where: {
        CheckinDate: {
          gte: startDate,
          lt: endDate,
        },
        IsPaid: true,
      },
    });

    const monthlyRevenue = Array(12).fill(0);

    bookings.forEach((booking) => {
      const month = booking.CheckinDate.getMonth();
      monthlyRevenue[month] += parseFloat(booking.TotalPrice.toFixed(2));
    });

    return monthlyRevenue.map((total, index) => ({
      month: index + 1,
      totalRevenue: total,
    }));
  }

  async getRevenueByRoomType(year: number): Promise<any> {
    const startDate = new Date(year, 0, 1);
    const endDate = new Date(year + 1, 0, 1);

    const bookings = await this.prisma.booking.findMany({
      where: {
        CheckinDate: {
          gte: startDate,
          lt: endDate,
        },
        IsPaid: true,
      },
      include: {
        Room: {
          include: {
            RoomType: true,
          },
        },
      },
    });

    const revenueByRoomType = bookings.reduce((acc, booking) => {
      const roomType = booking.Room.RoomType.Name; // Access TypeRoom's Name property
      const totalPrice = parseFloat(booking.TotalPrice.toFixed(2));

      if (!acc[roomType]) {
        acc[roomType] = 0;
      }

      acc[roomType] += totalPrice;
      return acc;
    }, {});

    // Convert object to array
    const revenueArray = Object.entries(revenueByRoomType).map(
      ([roomType, revenue]) => ({
        roomType,
        revenue,
      }),
    );

    return revenueArray;
  }

  //lay doanh thu cac loai phong theo thang
  async getRevenueByRoomTypeByMonth(month: number, year: number): Promise<any> {
    const startDate = new Date(year, month - 1, 1);
    const endDate = new Date(year, month, 1); // last day of the month
    
    const bookings = await this.prisma.booking.findMany({
      where: {
        CheckinDate: {
          gte: startDate,
          lt: endDate,
        },
        IsPaid: true,
      },
      include: {
        Room: {
          include: {
            RoomType: true, // Include TypeRoom data for each Room
          },
        },
      },
    });

    const revenueByRoomType = bookings.reduce((acc, booking) => {
      const roomType = booking.Room.RoomType.Name; // Access TypeRoom's Name property
      const totalPrice = parseFloat(booking.TotalPrice.toFixed(2));

      if (!acc[roomType]) {
        acc[roomType] = 0;
      }

      acc[roomType] += totalPrice;
      return acc;
    }, {});

    // Convert object to array
    const revenueArray = Object.entries(revenueByRoomType).map(
      ([roomType, revenue]) => ({
        roomType,
        revenue,
      }),
    );

    return revenueArray;
  }
}
