import { Request, Response } from 'express';
import { ReportService } from './report.service';

import {
  Controller,
  Get,
  Query,
  ParseIntPipe,
  Param,
  Req,
  Res,
} from '@nestjs/common';
@Controller('api/v1/report')
export class ReportController {
  constructor(private readonly reportService: ReportService) {}

  //lấy doanh thu theo tháng

  @Get()
  async getRevenueByMonthAndYear(
    @Query('month', ParseIntPipe) month: number,
    @Query('year', ParseIntPipe) year: number,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    // return this.reportService.getRevenueByMonthAndYear(month, year);
    const result = await this.reportService.getRevenueByMonthAndYear(
      month,
      year,
    );
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
  @Get('/yearly-revenue')
  async getRevenueByYear(
    @Query('year', ParseIntPipe) year: number,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    try {
      const result = await this.reportService.getRevenueByYear(year);
      return res.status(200).json({
        status: '200',
        message: 'Successfully!',
        content: result,
      });
    } catch (error) {
      return res.status(500).json({
        status: '500',
        message: 'Error fetching revenue data',
        error: error.message,
      });
    }
  }

  @Get('/yearly-revenue/room-type')
  async getRevenueByRoomType(
    @Query('year', ParseIntPipe) year: number,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    try {
      const result = await this.reportService.getRevenueByRoomType(year);
      return res.status(200).json({
        status: '200',
        message: 'Successfully!',
        content: result,
      });
    } catch (error) {
      return res.status(500).json({
        status: '500',
        message: 'Error fetching revenue data',
        error: error.message,
      });
    }
  }

  @Get('/yearly-revenue/room-type-by-month')
  async getRevenueByRoomTypeByMonth(
    @Query('month', ParseIntPipe) month: number,
    @Query('year', ParseIntPipe) year: number,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    try {
      const result = await this.reportService.getRevenueByRoomTypeByMonth(
        month,
        year,
      );
      return res.status(200).json({
        status: '200',
        message: 'Successfully!',
        content: result,
      });
    } catch (error) {
      return res.status(500).json({
        status: '500',
        message: 'Error fetching revenue data',
        error: error.message,
      });
    }
  }
}
