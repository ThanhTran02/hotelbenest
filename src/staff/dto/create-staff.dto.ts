export class CreateStaffDto {
  Name: string;
  Position: string;
  DateOfBirth: string;
  Phone: string;
  Email: string;
}
