import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Req,
  Res,
  Put,
} from '@nestjs/common';
import { StaffService } from './staff.service';
import { CreateStaffDto } from './dto/create-staff.dto';
import { UpdateStaffDto } from './dto/update-staff.dto';
import { Request, Response } from 'express';

@Controller('api/v1/staff')
export class StaffController {
  constructor(private readonly staffService: StaffService) {}

  @Post()
  async create(
    @Body() createStaffDto: CreateStaffDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.staffService.create(createStaffDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get()
  async findAll(@Req() req: Request, @Res() res: Response): Promise<any> {
    const result = await this.staffService.findAll();
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get(':id')
  async findOne(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.staffService.findOne(+id);

    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateStaffDto: UpdateStaffDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.staffService.update(+id, updateStaffDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Delete(':id')
  async remove(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    await this.staffService.remove(+id);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
    });
  }
}
