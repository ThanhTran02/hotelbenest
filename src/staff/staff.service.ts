import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateStaffDto } from './dto/create-staff.dto';
import { UpdateStaffDto } from './dto/update-staff.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class StaffService {
  constructor(private prisma: PrismaService) {}

  create(createStaffDto: CreateStaffDto) {
    console.log(createStaffDto);

    return this.prisma.staff.create({ data: createStaffDto });
  }

  findAll() {
    return this.prisma.staff.findMany({ where: { IsDeleted: false } });
  }

  findOne(id: number) {
    return this.prisma.staff.findMany({
      where: { StaffID: id, IsDeleted: false },
    });
  }

  async update(id: number, updateStaffDto: UpdateStaffDto) {
    const checkId = await this.prisma.staff.findMany({
      where: { StaffID: id, IsDeleted: false },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.staff.update({
      where: { StaffID: id, IsDeleted: false },
      data: updateStaffDto,
    });
  }

  async remove(id: number) {
    const checkId = await this.prisma.staff.findUnique({
      where: { StaffID: id, IsDeleted: false },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.staff.update({
      where: { StaffID: id },
      data: { IsDeleted: true },
    });
  }
}
