import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateRegulationDto } from './dto/create-regulation.dto';
import { UpdateRegulationDto } from './dto/update-regulation.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class RegulationService {
  constructor(private prisma: PrismaService) {}
  create(createRegulationDto: CreateRegulationDto) {
    return 'This action adds a new regulation';
  }

  findAll() {
    return this.prisma.regulation.findMany();
  }

  findOne(id: number) {
    return this.prisma.regulation.findFirst({ where: { RegulationID: id } });
  }

  async update(id: number, updateRegulationDto: UpdateRegulationDto) {
    const checkId = await this.prisma.regulation.findFirst({
      where: { RegulationID: id },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.regulation.update({
      where: { RegulationID: id },
      data: updateRegulationDto,
    });
  }

  remove(id: number) {
    return `This action removes a #${id} regulation`;
  }
}
