import { Module } from '@nestjs/common';
import { RegulationService } from './regulation.service';
import { RegulationController } from './regulation.controller';
import { PrismaService } from 'src/prisma.service';

@Module({
  controllers: [RegulationController],
  providers: [RegulationService, PrismaService],
})
export class RegulationModule {}
