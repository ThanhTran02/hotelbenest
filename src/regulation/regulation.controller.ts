import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Req,
  Res,
  Put,
} from '@nestjs/common';
import { RegulationService } from './regulation.service';
import { UpdateRegulationDto } from './dto/update-regulation.dto';
import { Request, Response } from 'express';

@Controller('api/v1/regulation')
export class RegulationController {
  constructor(private readonly regulationService: RegulationService) {}

  @Get()
  async findAll(@Req() req: Request, @Res() res: Response): Promise<any> {
    const result = await this.regulationService.findAll();
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
  @Get(':id')
  async findOne(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.regulationService.findOne(+id);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateRegulationDto: UpdateRegulationDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.regulationService.update(
      +id,
      updateRegulationDto,
    );
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
}
