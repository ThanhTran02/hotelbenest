import { BadRequestException, Injectable } from '@nestjs/common';
import { CreatePaymentDto } from './dto/create-payment.dto';
import { UpdatePaymentDto } from './dto/update-payment.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class PaymentService {
  constructor(private prisma: PrismaService) {}
  create(createPaymentDto: any) {
    return this.prisma.payment.create({ data: createPaymentDto });
  }

  findAll() {
    return this.prisma.payment.findMany();
  }

  findOne(id: number) {
    return this.prisma.payment.findFirst({ where: { BookingID: id } });
  }

  async update(id: number, updatePaymentDto: any) {
    const checkId = await this.prisma.payment.findUnique({
      where: { PaymentID: id },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.payment.update({
      where: { PaymentID: id },
      data: updatePaymentDto,
    });
  }

  async remove(id: number) {
    const checkId = await this.prisma.payment.findUnique({
      where: { PaymentID: id },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.payment.delete({ where: { PaymentID: id } });
  }
}
