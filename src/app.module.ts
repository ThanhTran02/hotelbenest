import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GuestModule } from './guest/guest.module';
import { BookingModule } from './booking/booking.module';
import { GuestTypeModule } from './guest-type/guest-type.module';
import { PaymentModule } from './payment/payment.module';
import { RegulationModule } from './regulation/regulation.module';
import { RoomModule } from './room/room.module';
import { RoomTypeModule } from './room-type/room-type.module';
import { StaffModule } from './staff/staff.module';
import { ReportModule } from './report/report.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [GuestModule, BookingModule, GuestTypeModule, PaymentModule, RegulationModule, RoomModule, RoomTypeModule, StaffModule, ReportModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
