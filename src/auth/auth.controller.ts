import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { Response } from 'express';

@Controller('api/v1/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async Login(@Body() loginAuth: Login, @Res() res: Response): Promise<any> {
    const result = await this.authService.login(loginAuth);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Post('sign-up')
  async signUp(@Body() body, @Res() response: Response): Promise<any> {
    const result = await this.authService.signUp(body);
    return response.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
}
