import {
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { PrismaService } from 'src/prisma.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwtService: JwtService,
  ) {}
  async login(loginData: Login) {
    const { Email, Password } = loginData;
    const checkStaff = await this.prisma.staff.findFirst({
      where: { Email, Password, IsDeleted: false },
    });

    if (checkStaff)
      return { token: this.jwtService.sign({ ...checkStaff, role: 'admin' }) };

    const checkGuest = await this.prisma.guest.findFirst({
      where: { Email, IsDeleted: false },
    });

    if (!checkGuest) {
      throw new NotFoundException('User not found!');
    }

    if (!(await bcrypt.compare(Password, checkGuest.Password))) {
      throw new NotFoundException('Invalid password!');
    }

    if (checkGuest)
      return { token: this.jwtService.sign({ ...checkGuest, role: 'user' }) };
  }
  async signUp(body: any): Promise<any> {
    const { Email, Password } = body;
    const findEmail = await this.prisma.guest.findFirst({
      where: { Email, IsDeleted: false },
    });
    if (findEmail) {
      throw new UnprocessableEntityException(`Email ${Email} already exists!`);
    } else {
      const hashedPassword = await bcrypt.hash(Password, 10);
      return this.prisma.guest.create({
        data: { ...body, Password: hashedPassword },
      });
    }
  }
}
