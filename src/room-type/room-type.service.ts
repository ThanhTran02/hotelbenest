import { BadRequestException, Injectable } from '@nestjs/common';

import { PrismaService } from 'src/prisma.service';

@Injectable()
export class typeRoomService {
  constructor(private prisma: PrismaService) {}

  create(createtypeRoomDto: any) {
    console.log(createtypeRoomDto);

    return this.prisma.typeRoom.create({ data: createtypeRoomDto });
  }

  findAll() {
    return this.prisma.typeRoom.findMany();
  }

  findOne(id: number) {
    return this.prisma.typeRoom.findFirst({ where: { TypeRoomID: id } });
  }

  async update(id: number, updatetypeRoomDto: any) {
    console.log(id, updatetypeRoomDto);

    const checkId = await this.prisma.typeRoom.findUnique({
      where: { TypeRoomID: id },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.typeRoom.update({
      where: { TypeRoomID: id },
      data: updatetypeRoomDto,
    });
  }

  async remove(id: number) {
    const checkId = await this.prisma.typeRoom.findUnique({
      where: { TypeRoomID: id },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.typeRoom.delete({ where: { TypeRoomID: id } });
  }
}
