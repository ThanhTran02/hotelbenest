import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  Res,
  Put,
} from '@nestjs/common';
import { typeRoomService } from './room-type.service';
import { CreateRoomTypeDto } from './dto/create-room-type.dto';
import { UpdateRoomTypeDto } from './dto/update-room-type.dto';
import { Request, Response } from 'express';

@Controller('api/v1/room-type')
export class RoomTypeController {
  constructor(private readonly roomTypeService: typeRoomService) {}
  @Post()
  async create(
    @Body() createRoomTypeDto: CreateRoomTypeDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.roomTypeService.create(createRoomTypeDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get()
  async findAll(@Req() req: Request, @Res() res: Response): Promise<any> {
    const result = await this.roomTypeService.findAll();
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get(':id')
  async findOne(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.roomTypeService.findOne(+id);

    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateRoomTypeDto: UpdateRoomTypeDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    console.log(id);

    const result = await this.roomTypeService.update(+id, updateRoomTypeDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Delete(':id')
  async remove(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    await this.roomTypeService.remove(+id);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
    });
  }
}
