import { Module } from '@nestjs/common';
import { typeRoomService } from './room-type.service';
import { RoomTypeController } from './room-type.controller';
import { PrismaService } from 'src/prisma.service';

@Module({
  controllers: [RoomTypeController],
  providers: [typeRoomService, PrismaService],
})
export class RoomTypeModule {}
