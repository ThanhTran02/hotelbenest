import { BadRequestException, Injectable } from '@nestjs/common';
import { UpdateBookingDto } from './dto/update-booking.dto';
import { PrismaService } from 'src/prisma.service';
import { CreateBookingDto } from './dto/create-booking.dto';

@Injectable()
export class BookingService {
  constructor(private prisma: PrismaService) {}

  async create(createBookingDto: CreateBookingDto) {
    const { RoomID, GuestID, CheckinDate, CheckoutDate, users, TotalPrice } =
      createBookingDto;
    console.log(users);

    // Bắt đầu một transaction
    const result = await this.prisma.$transaction(async (prisma) => {
      // Tạo bản ghi cho bảng Booking
      const booking = await prisma.booking.create({
        data: {
          RoomID, // Giả định RoomNumber là RoomID
          CheckinDate: new Date(CheckinDate),
          CheckoutDate: new Date(CheckoutDate),
          TotalPrice,
          GuestID,
        },
      });

      for (const user of users) {
        // Tạo hoặc tìm kiếm Guest
        console.log(user);

        const guest = await prisma.guest.create({
          data: {
            Name: user.Name,
            Address: user.Address,
            Cmnd: user.Cmnd,
            TypeGuestID: user.TypeGuestID,
          },
        });

        // Tạo bản ghi trong bảng BookingGuest
        await prisma.bookingGuest.create({
          data: {
            BookingID: booking.BookingID,
            GuestID: guest.GuestID,
          },
        });
      }

      return booking;
    });

    return result;
  }

  findAll() {
    return this.prisma.booking.findMany({
      include: { BookingGuest: { include: { Guest: true } } },
    });
  }
  searchBookingByUserName(name: string) {
    return this.prisma.booking.findMany({
      where: {
        BookingGuest: {
          some: {
            Guest: {
              Name: {
                contains: name,
              },
            },
          },
        },
      },
      include: { BookingGuest: { include: { Guest: true } } },
    });
  }

  findOne(id: number) {
    return this.prisma.booking.findMany({
      where: {
        BookingID: id,
      },
      include: { BookingGuest: { include: { Guest: true } } },
    });
  }

  findBookingById(id: number) {
    return this.prisma.booking.findMany({
      where: {
        GuestID: id,
      },
      include: { BookingGuest: { include: { Guest: true } } },
    });
  }

  async update(id: number, updateBookingDto: UpdateBookingDto) {
    const checkId = await this.prisma.booking.findUnique({
      where: { BookingID: id },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.booking.update({
      where: { BookingID: id },
      data: updateBookingDto,
    });
  }

  async remove(id: number) {
    const checkId = await this.prisma.booking.findUnique({
      where: { BookingID: id },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.booking.delete({ where: { BookingID: id } });
  }
}
