import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  Res,
  Put,
  Query,
} from '@nestjs/common';
import { BookingService } from './booking.service';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';
import { Request, Response } from 'express';
@Controller('api/v1/booking')
export class BookingController {
  constructor(private readonly bookingService: BookingService) {}

  @Post()
  async create(
    @Body() createBookingDto: CreateBookingDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.bookingService.create(createBookingDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get()
  async findAll(@Req() req: Request, @Res() res: Response): Promise<any> {
    const result = await this.bookingService.findAll();
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
  @Get('search')
  async searchBookingByUserName(
    @Query('name') name: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.bookingService.searchBookingByUserName(name);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get('search-by-id/:id')
  async searchByID(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.bookingService.findBookingById(+id);

    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
  @Get(':id')
  async findOne(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.bookingService.findOne(+id);

    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateBookingDto: UpdateBookingDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.bookingService.update(+id, updateBookingDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Delete(':id')
  async remove(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    await this.bookingService.remove(+id);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
    });
  }
}
