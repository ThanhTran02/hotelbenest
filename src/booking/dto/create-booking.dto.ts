export class CreateBookingDto {
  RoomID: number;
  CheckinDate: string;
  CheckoutDate: string;
  TotalPrice: number;
  GuestID: number;
  users: {
    Name: string;
    Address: string;
    Cmnd: string;
    TypeGuestID: number;
  }[];
}
