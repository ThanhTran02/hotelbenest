import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  Res,
  Put,
  Query,
} from '@nestjs/common';
import { RoomService } from './room.service';
import { CreateRoomDto } from './dto/create-room.dto';
import { UpdateRoomDto } from './dto/update-room.dto';
import { Response } from 'express';

@Controller('api/v1/room')
export class RoomController {
  constructor(private readonly roomService: RoomService) {}

  @Post()
  async create(
    @Body() createRoomDto: CreateRoomDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.roomService.create(createRoomDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get()
  async findAll(@Req() req: Request, @Res() res: Response): Promise<any> {
    const result = await this.roomService.findAll();
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get('/all-id-room')
  async findAllIDRoom(@Req() req: Request, @Res() res: Response): Promise<any> {
    const result = await this.roomService.findAllIDRoom();
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
  // findAllIDRoom() {

  //   return this.roomService.findAll();
  // }
  @Get('search')
  async searchRoomByID(
    @Query('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.roomService.searchRoomByID(id);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }
  @Get(':id')
  async findOne(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.roomService.findOne(+id);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateRoomDto: UpdateRoomDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    console.log(id);

    const result = await this.roomService.update(+id, updateRoomDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.roomService.remove(+id);
  }
}
