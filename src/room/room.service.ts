import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateRoomDto } from './dto/create-room.dto';
import { UpdateRoomDto } from './dto/update-room.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class RoomService {
  constructor(private prisma: PrismaService) {}
  create(createRoomDto: any) {
    console.log(createRoomDto);

    return this.prisma.room.create({ data: createRoomDto });
  }

  findAll() {
    return this.prisma.room.findMany({ include: { RoomType: true } });
  }
  findAllIDRoom() {
    return this.prisma.room.findMany({
      where: { Status: true },
      select: {
        RoomID: true,
      },
    });
  }

  findOne(id: number) {
    return this.prisma.room.findFirst({
      where: { RoomID: id },
      include: { RoomType: true },
    });
  }

  async searchRoomByID(id: any) {
    console.log(id);

    const allRooms = await this.prisma.room.findMany({
      include: { RoomType: true },
    });

    const filteredRooms = allRooms.filter((room) =>
      room.RoomID.toString().includes(id.toString()),
    );

    return filteredRooms;
  }

  async update(id: number, updateRoomDto: any) {
    const checkId = await this.prisma.room.findUnique({
      where: { RoomID: id },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.room.update({
      where: { RoomID: id },
      data: updateRoomDto,
    });
  }

  remove(id: number) {
    return `This action removes a #${id} room`;
  }
}
