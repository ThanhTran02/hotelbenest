import { Module } from '@nestjs/common';
import { GuestTypeService } from './guest-type.service';
import { GuestTypeController } from './guest-type.controller';
import { PrismaService } from 'src/prisma.service';

@Module({
  controllers: [GuestTypeController],
  providers: [GuestTypeService, PrismaService],
})
export class GuestTypeModule {}
