import { PartialType } from '@nestjs/mapped-types';
import { GuestTypeDto } from './guest-type.dto';

export class UpdateGuestTypeDto extends PartialType(GuestTypeDto) {}
