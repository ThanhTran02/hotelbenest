import { BadRequestException, Injectable } from '@nestjs/common';
import { GuestTypeDto } from './dto/guest-type.dto';
import { UpdateGuestTypeDto } from './dto/update-guest-type.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class GuestTypeService {
  constructor(private prisma: PrismaService) {}
  create(createGuestTypeDto: GuestTypeDto) {
    return this.prisma.guestType.create({ data: createGuestTypeDto });
  }

  findAll() {
    return this.prisma.guestType.findMany();
  }

  findOne(id: number) {
    return this.prisma.guestType.findMany({ where: { GuestTypeID: id } });
  }

  async update(id: number, updateGuestTypeDto: UpdateGuestTypeDto) {
    const checkId = await this.prisma.guestType.findUnique({
      where: { GuestTypeID: id },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.guestType.update({
      where: { GuestTypeID: id },
      data: updateGuestTypeDto,
    });
  }

  async remove(id: number) {
    // return `This action removes a #${id} guestType`;
    const checkId = await this.prisma.guestType.findUnique({
      where: { GuestTypeID: id },
    });
    if (!checkId) throw new BadRequestException('No Found id!');
    return this.prisma.guestType.delete({ where: { GuestTypeID: id } });
  }
}
