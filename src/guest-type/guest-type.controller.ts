import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  Res,
  Put,
} from '@nestjs/common';
import { GuestTypeService } from './guest-type.service';
import { GuestTypeDto } from './dto/guest-type.dto';
import { UpdateGuestTypeDto } from './dto/update-guest-type.dto';
import { Request, Response } from 'express';

@Controller('api/v1/guest-type')
export class GuestTypeController {
  constructor(private readonly guestTypeService: GuestTypeService) {}

  @Post()
  async create(
    @Body() createGuestTypeDto: GuestTypeDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.guestTypeService.create(createGuestTypeDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get()
  async findAll(@Req() req: Request, @Res() res: Response): Promise<any> {
    const result = await this.guestTypeService.findAll();
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Get(':id')
  async findOne(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.guestTypeService.findOne(+id);

    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateGuestTypeDto: UpdateGuestTypeDto,
  // ) {
  //   return this.guestTypeService.update(+id, updateGuestTypeDto);
  // }
  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateGuestTypeDto: UpdateGuestTypeDto,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    const result = await this.guestTypeService.update(+id, updateGuestTypeDto);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
      content: result,
    });
  }

  @Delete(':id')
  async remove(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    await this.guestTypeService.remove(+id);
    return res.status(200).json({
      status: '200',
      message: 'Successfully!',
    });
  }
}
